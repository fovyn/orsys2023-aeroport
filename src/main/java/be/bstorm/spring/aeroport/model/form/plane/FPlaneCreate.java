package be.bstorm.spring.aeroport.model.form.plane;


import be.bstorm.spring.aeroport.model.entity.Plane;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@Builder
public record FPlaneCreate(

        @NotBlank(message = "Immatriculation is required") String imma,
        Long ownerId,
        Long typeId
) {

    public Plane toBll() {
        Plane plane = new Plane();

        plane.setImmatriculationNumber(imma);

        return plane;
    }
}
