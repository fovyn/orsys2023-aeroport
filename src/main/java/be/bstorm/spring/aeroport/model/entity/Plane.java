package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Plane extends BaseEntity {
    @Column(unique = true)
    private String immatriculationNumber;

    @ManyToOne(targetEntity = PlaneType.class, optional = false, cascade = {CascadeType.DETACH})
    private PlaneType type;

    @ManyToOne(targetEntity = Person.class)
    private Person owner;
}
