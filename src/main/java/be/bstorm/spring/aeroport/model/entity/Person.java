package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Inheritance(strategy = InheritanceType.JOINED)
public class Person extends BaseEntity {
    private String name;
    private String phoneNumber;
    @Embedded
    private Address address;

//    @OneToMany(targetEntity = Plane.class, mappedBy = "owner")
//    private List<Plane> planes = new ArrayList<>();
}
