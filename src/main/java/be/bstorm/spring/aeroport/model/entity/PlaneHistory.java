package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class PlaneHistory extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Plane plane;
    @ManyToOne
    private Person seller;
    @ManyToOne
    private Person buyer;
}
