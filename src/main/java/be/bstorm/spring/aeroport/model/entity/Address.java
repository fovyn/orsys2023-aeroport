package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class Address {
    @Column(name = "address_street")
    private String street;
    @Column(name = "address_number")
    private String number;
    @Column(name = "address_postalCode")
    private String postalCode = "1000";
    @Column(name = "address_country")
    private String country = "Belgium";
}
