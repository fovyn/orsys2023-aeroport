package be.bstorm.spring.aeroport.model.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class ExceptionDTO {
    private int statusCode;
    private HttpStatus status;
    private Object errors;
}
