package be.bstorm.spring.aeroport.model.form.security;

import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@Builder
public record FUserRegister(
        @NotBlank String username,
        @NotBlank String password
) {

    public SecurityUser toBll() {
        return SecurityUser.builder()
                .username(username)
                .password(password)
                .build();
    }
}
