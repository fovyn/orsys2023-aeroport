package be.bstorm.spring.aeroport.model.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Mechanic extends Person {

    @ManyToMany(targetEntity = PlaneType.class)
    private Set<PlaneType> certifications = new HashSet<>();
}
