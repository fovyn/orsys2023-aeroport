package be.bstorm.spring.aeroport.model.dto.security;

import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import lombok.Builder;

@Builder
public record UserRegisterDTO(
        String token,
        SecurityUser user
) {
}
