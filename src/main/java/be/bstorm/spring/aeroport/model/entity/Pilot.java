package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Pilot extends Person {
    @Column(unique = true)
    private String patentNumber;

    @ManyToMany(targetEntity = PlaneType.class, cascade = {CascadeType.DETACH})
    private Set<PlaneType> certifications = new HashSet<>();
}
