package be.bstorm.spring.aeroport.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Time;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Maintenance extends BaseEntity {

    @ManyToOne(targetEntity = Plane.class)
    private Plane plane;
    @ManyToOne(targetEntity = Mechanic.class)
    private Mechanic repairer;
    @ManyToOne(targetEntity = Mechanic.class)
    private Mechanic verifier;

    private String purpose;
    private Time duration;
}
