package be.bstorm.spring.aeroport.config;

import be.bstorm.spring.aeroport.exception.AlreadyExistException;
import be.bstorm.spring.aeroport.exception.NotFoundException;
import be.bstorm.spring.aeroport.exception.PreConditionException;
import be.bstorm.spring.aeroport.model.dto.ExceptionDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<ExceptionDTO> notFoundExceptionHandler(NotFoundException exception) {
        ExceptionDTO dto = ExceptionDTO.builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .status(HttpStatus.NOT_FOUND)
                .errors(Collections.singletonList(exception.getMessage()))
                .build();

        return ResponseEntity.status(dto.getStatus()).body(dto);
    }

    @ExceptionHandler(value = {PreConditionException.class})
    public ResponseEntity<ExceptionDTO> preConditionExceptionHandler(PreConditionException exception) {
        Map<String, List<String>> errors = exception.getErrors().stream()
                .collect(
                        Collectors.groupingBy(
                                FieldError::getField,
                                Collectors.mapping(FieldError::getDefaultMessage, Collectors.toList())
                        )
                );

        ExceptionDTO dto = ExceptionDTO.builder()
                .statusCode(HttpStatus.PRECONDITION_FAILED.value())
                .status(HttpStatus.PRECONDITION_FAILED)
                .errors(errors)
                .build();

        return ResponseEntity
                .status(HttpStatus.PRECONDITION_FAILED)
                .body(dto);
    }

    @ExceptionHandler(value = {AlreadyExistException.class})
    public ResponseEntity<ExceptionDTO> alreadyExistException(AlreadyExistException exception) {
        ExceptionDTO dto = ExceptionDTO.builder()
                .statusCode(HttpStatus.CONFLICT.value())
                .status(HttpStatus.CONFLICT)
                .errors(Collections.singletonList(exception.getMessage()))
                .build();

        return ResponseEntity.status(dto.getStatus()).body(dto);
    }
}
