package be.bstorm.spring.aeroport.config;

import be.bstorm.spring.aeroport.config.jwt.JwtFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Slf4j
@EnableMethodSecurity
public class SecurityConfig {
//    private final OpenLdapConfig ldapConfig;
//
//    public SecurityConfig(OpenLdapConfig ldapConfig) {
//        this.ldapConfig = ldapConfig;
//    }
//
//    @Bean
//    public LdapContextSource ldapContextSource() {
//        LdapContextSource contextSource = new LdapContextSource();
//
//        contextSource.setUrl(ldapConfig.getUrls());
//        contextSource.setBase(ldapConfig.getBaseDn());
//        contextSource.setUserDn(ldapConfig.getUsername());
//        contextSource.setPassword(ldapConfig.getPassword());
//
//        return contextSource;
//    }

//    @Bean
//    public LdapTemplate ldapTemplate() {
//        return new LdapTemplate(ldapContextSource());
//    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration configuration
    ) throws Exception {
        return configuration.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
//        UserDetails admin = User.builder()
//                .username("Admin")
//                .password(passwordEncoder.encode("Admin"))
//                .roles("ADMIN", "USER")
//                .build();
//
//        UserDetails user = User.builder()
//                .username("User")
//                .password(passwordEncoder.encode("User"))
//                .roles("USER")
//                .build();
//
//        return new InMemoryUserDetailsManager(admin, user);
//    }

    @Bean
    public SecurityFilterChain filterChain(
            HttpSecurity http,
            JwtFilter filter
    ) throws Exception {
        http
                .cors().disable()
                .csrf().disable()
                .authorizeRequests(registry -> {
                    registry.antMatchers("/swagger-ui/**", "/swagger-ui.html", "/v3/**").permitAll();

                    try {
                        registry.antMatchers("/h2-console/**").permitAll().and()
                                .headers().frameOptions().disable().and()
                                .csrf().ignoringAntMatchers("/h2-console/**");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                    registry.antMatchers("/sign-in", "/login", "/register").permitAll();
                    try {
                        registry.anyRequest().authenticated().and()
                                .sessionManagement(sm -> sm.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });

        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
}
