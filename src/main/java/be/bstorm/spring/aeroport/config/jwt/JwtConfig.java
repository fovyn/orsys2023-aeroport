package be.bstorm.spring.aeroport.config.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.crypto.SecretKey;

@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtConfig {

    private SignatureAlgorithm algorithm = SignatureAlgorithm.HS512;
    private int expirationTime = 24 * 3600 * 1000;

    private SecretKey secretKey = Keys.secretKeyFor(algorithm);
}
