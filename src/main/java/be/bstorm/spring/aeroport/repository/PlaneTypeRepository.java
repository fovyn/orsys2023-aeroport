package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.PlaneType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaneTypeRepository extends JpaRepository<PlaneType, Long> {
}
