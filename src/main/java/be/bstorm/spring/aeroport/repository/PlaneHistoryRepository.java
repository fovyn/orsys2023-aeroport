package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.PlaneHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaneHistoryRepository extends JpaRepository<PlaneHistory, Long> {

    @Query("SELECT ph FROM PlaneHistory ph JOIN Plane p WHERE p.id = :id")
    List<PlaneHistory> findHistoryByPlaneId(@Param("id") Long id);
    @Query("SELECT ph FROM PlaneHistory ph JOIN Plane p WHERE p.immatriculationNumber = :immatriculationNumber")
    List<PlaneHistory> findHistoryByPlaneImmatriculationNumber(@Param("immatriculationNumber") String immatriculationNumber);
}
