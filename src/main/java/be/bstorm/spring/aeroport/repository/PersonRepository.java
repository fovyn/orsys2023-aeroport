package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
}
