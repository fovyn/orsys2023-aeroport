package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.Pilot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PilotRepository extends JpaRepository<Pilot, Long> {
}
