package be.bstorm.spring.aeroport.repository.security;

import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<SecurityUser, Long> {
    @Query(value = "SELECT su FROM SecurityUser su WHERE su.username = :username")
    Optional<SecurityUser> findOneByUsername(@Param("username") String username);
}
