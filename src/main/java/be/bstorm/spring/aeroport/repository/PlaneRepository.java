package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.model.entity.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlaneRepository extends JpaRepository<Plane, Long> {

    @Query(value = "SELECT p FROM Plane p JOIN p.owner o WHERE o = :owner")
    List<Plane> findAllByOwner(@Param("owner") Person owner);

    @Query(value = "SELECT p FROM Plane p WHERE p.immatriculationNumber = :immatriculation")
    Optional<Plane> findByImmatriculation(@Param("immatriculation") String immatriculation);
}
