package be.bstorm.spring.aeroport.repository;

import be.bstorm.spring.aeroport.model.entity.Maintenance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface MaintenanceRepository extends JpaRepository<Maintenance, Long> {

    Page<Maintenance> findAllByCreatedAt(LocalDate createdAt, Pageable pageable);
}
