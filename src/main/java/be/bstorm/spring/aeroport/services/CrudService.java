package be.bstorm.spring.aeroport.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Optional;

public interface CrudService<TBll, TKey> {

    default void merge(TBll target, Object source) {
        Class<TBll> targetClass = (Class<TBll>) target.getClass();
        Class<?> sourceClass = source.getClass();

        Field[] targetFields = targetClass.getDeclaredFields();
        Field[] sourceFields = sourceClass.getDeclaredFields();

        for (Field tField : targetFields) {
            if (tField.getName().equals("id")) continue;
            tField.setAccessible(true);
            Optional<Field> opt = Arrays.stream(sourceFields).filter(it -> it.getName().equals(tField.getName())).findFirst();

            opt.ifPresent(sField -> {
                sField.setAccessible(true);
                try {
                    Object value = sField.get(source);
                    tField.set(target, value);
                } catch (IllegalAccessException e) {
                }
            });
        }
    }

    TBll create(TBll entity);

    Page<TBll> findAll(Pageable pageable);

    Optional<TBll> findOneById(TKey id);

    TBll update(TKey id, TBll entity);

    void delete(TBll entity);
}
