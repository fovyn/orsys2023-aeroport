package be.bstorm.spring.aeroport.services.plane;

import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.model.entity.Plane;
import be.bstorm.spring.aeroport.model.entity.PlaneHistory;
import be.bstorm.spring.aeroport.repository.PlaneHistoryRepository;
import be.bstorm.spring.aeroport.repository.PlaneRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlaneServiceImpl implements PlaneService {
    private final PlaneRepository planeRepository;
    private final PlaneHistoryRepository planeHistoryRepository;

    public PlaneServiceImpl(
            PlaneRepository planeRepository,
            PlaneHistoryRepository planeHistoryRepository
    ) {
        this.planeRepository = planeRepository;
        this.planeHistoryRepository = planeHistoryRepository;
    }

    @Override
    public Plane create(Plane entity) {
        return this.planeRepository.save(entity);
    }

    @Override
    public Page<Plane> findAll(Pageable pageable) {
        return this.planeRepository.findAll(pageable);
    }

    @Override
    public Optional<Plane> findOneById(Long id) {
        return this.planeRepository.findById(id);
    }

    @Override
    public Plane update(Long id, Plane entity) {
        Plane toUpdate = this.planeRepository.findById(id).orElseThrow();

        if (!toUpdate.getOwner().equals(entity.getOwner())) {
            this.sellPlane(toUpdate, toUpdate.getOwner(), entity.getOwner());
        }

        merge(toUpdate, entity);

        return this.planeRepository.save(toUpdate);
    }

    @Override
    public void delete(Plane entity) {
        entity.setActive(false);
        this.planeRepository.save(entity);
    }

    @Override
    public void sellPlane(Plane plane, Person seller, Person buyer) {
        PlaneHistory ph = new PlaneHistory();
        ph.setPlane(plane);
        ph.setSeller(seller);
        ph.setBuyer(buyer);

        this.planeHistoryRepository.save(ph);

        plane.setOwner(buyer);
    }

    @Override
    public Optional<Plane> findByImmatriculation(String immatriculation) {
        return this.planeRepository.findByImmatriculation(immatriculation);
    }
}
