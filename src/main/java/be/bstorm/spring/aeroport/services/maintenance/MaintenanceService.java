package be.bstorm.spring.aeroport.services.maintenance;

import be.bstorm.spring.aeroport.model.entity.Maintenance;
import be.bstorm.spring.aeroport.model.entity.Mechanic;
import be.bstorm.spring.aeroport.model.entity.Plane;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.sql.Time;
import java.time.LocalDate;

public interface MaintenanceService {
    Maintenance create(Plane plane, Mechanic repairer, Mechanic checker, String purpose, Time duration);

    Page<Maintenance> showDateMaintenance(LocalDate date, Pageable pageable);
}
