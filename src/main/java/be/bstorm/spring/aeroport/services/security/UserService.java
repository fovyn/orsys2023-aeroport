package be.bstorm.spring.aeroport.services.security;

import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import be.bstorm.spring.aeroport.services.CrudService;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends CrudService<SecurityUser, Long>, UserDetailsService {
}
