package be.bstorm.spring.aeroport.services.plane;

import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.model.entity.Plane;
import be.bstorm.spring.aeroport.services.CrudService;

import java.util.Optional;

public interface PlaneService extends CrudService<Plane, Long> {
    void sellPlane(Plane plane, Person seller, Person buyer);

    Optional<Plane> findByImmatriculation(String immatriculation);
}
