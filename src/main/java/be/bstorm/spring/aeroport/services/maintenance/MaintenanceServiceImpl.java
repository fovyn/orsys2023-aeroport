package be.bstorm.spring.aeroport.services.maintenance;

import be.bstorm.spring.aeroport.model.entity.Maintenance;
import be.bstorm.spring.aeroport.model.entity.Mechanic;
import be.bstorm.spring.aeroport.model.entity.Plane;
import be.bstorm.spring.aeroport.model.exception.BusinessRuleException;
import be.bstorm.spring.aeroport.repository.MaintenanceRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.LocalDate;

@Service
public class MaintenanceServiceImpl implements MaintenanceService {
    private final MaintenanceRepository maintenanceRepository;

    public MaintenanceServiceImpl(MaintenanceRepository maintenanceRepository) {
        this.maintenanceRepository = maintenanceRepository;
    }

    @Override
    public Maintenance create(Plane plane, Mechanic repairer, Mechanic checker, String purpose, Time duration) {
        if (repairer.equals(checker))
            throw new BusinessRuleException("The repairer and the verifier must not be the same");
        if (duration.getTime() <= 0)
            throw new BusinessRuleException("Duration must be gt 0 minutes");

        Maintenance newMaintenance = new Maintenance();
        newMaintenance.setPlane(plane);
        newMaintenance.setRepairer(repairer);
        newMaintenance.setVerifier(checker);
        newMaintenance.setPurpose(purpose);
        newMaintenance.setDuration(duration);

        return this.maintenanceRepository.save(newMaintenance);
    }

    @Override
    public Page<Maintenance> showDateMaintenance(LocalDate date, Pageable pageable) {
        return this.maintenanceRepository.findAllByCreatedAt(date, pageable);
    }
}
