package be.bstorm.spring.aeroport.services.security;

import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import be.bstorm.spring.aeroport.repository.security.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public SecurityUser create(SecurityUser entity) {
        return this.userRepository.save(entity);
    }

    @Override
    public Page<SecurityUser> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public Optional<SecurityUser> findOneById(Long id) {
        return Optional.empty();
    }

    @Override
    public SecurityUser update(Long id, SecurityUser entity) {
        return null;
    }

    @Override
    public void delete(SecurityUser entity) {

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.userRepository
                .findOneByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username doesn't exist"));
    }
}
