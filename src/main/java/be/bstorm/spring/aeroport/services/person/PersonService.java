package be.bstorm.spring.aeroport.services.person;

import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.services.CrudService;

public interface PersonService extends CrudService<Person, Long> {
}
