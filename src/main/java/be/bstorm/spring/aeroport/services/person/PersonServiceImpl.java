package be.bstorm.spring.aeroport.services.person;

import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.repository.PersonRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    @Override
    public Person create(Person entity) {
        return this.personRepository.save(entity);
    }

    @Override
    public Page<Person> findAll(Pageable pageable) {
        return this.personRepository.findAll(pageable);
    }

    @Override
    public Optional<Person> findOneById(Long id) {
        return this.personRepository.findById(id);
    }

    @Override
    public Person update(Long id, Person entity) {
        Person toUpdate = this.personRepository.findById(id).orElseThrow();

        merge(toUpdate, entity);

        return this.personRepository.save(toUpdate);
    }

    @Override
    public void delete(Person entity) {
        entity.setActive(false);
        this.personRepository.save(entity);
    }
}
