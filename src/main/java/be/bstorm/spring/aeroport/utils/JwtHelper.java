package be.bstorm.spring.aeroport.utils;

import be.bstorm.spring.aeroport.config.jwt.JwtConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtHelper {
    private final JwtConfig config;
    private final JwtParser parser;
    private final JwtBuilder builder;

    public JwtHelper(JwtConfig config) {
        this.config = config;
        parser = Jwts.parserBuilder()
                .setSigningKey(config.getSecretKey())
                .build();

        builder = Jwts.builder().signWith(config.getSecretKey());
    }


    public String getUsernameFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getExpiration);
    }

    public boolean isExpire(String token) {
        Date eDate = this.getExpirationDateFromToken(token);

        return eDate.before(new Date());
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimResolver) {
        final Claims claims = getClaimsFromToken(token);

        return claimResolver.apply(claims);
    }

    private Claims getClaimsFromToken(String token) {
        return parser
                .parseClaimsJws(token) //jws si signé sinon jwt
                .getBody();
    }


    public boolean validateToken(String token, UserDetails userDetails) {
        boolean hasSameSubject = getUsernameFromToken(token).equals(userDetails.getUsername());
// Mettre ici la validation de votre token
        return hasSameSubject && !isExpire(token);
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();

        //Ajouter le contenu à la payload du token

        return generateToken(claims, userDetails.getUsername());
    }

    private String generateToken(Map<String, Object> claims, String username) {

        return builder
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + config.getExpirationTime()))
                .compact();
    }
}
