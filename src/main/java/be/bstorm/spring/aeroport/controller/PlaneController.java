package be.bstorm.spring.aeroport.controller;

import be.bstorm.spring.aeroport.exception.AlreadyExistException;
import be.bstorm.spring.aeroport.exception.NotFoundException;
import be.bstorm.spring.aeroport.exception.PreConditionException;
import be.bstorm.spring.aeroport.model.dto.DemoDTO;
import be.bstorm.spring.aeroport.model.dto.ExceptionDTO;
import be.bstorm.spring.aeroport.model.entity.Person;
import be.bstorm.spring.aeroport.model.entity.Plane;
import be.bstorm.spring.aeroport.model.entity.PlaneType;
import be.bstorm.spring.aeroport.model.form.plane.FPlaneCreate;
import be.bstorm.spring.aeroport.repository.PlaneTypeRepository;
import be.bstorm.spring.aeroport.services.person.PersonService;
import be.bstorm.spring.aeroport.services.plane.PlaneService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = {"/planes"})
public class PlaneController {

    //GET (/, /{id}) => Lecture (200 OK, 404 NOT_FOUND)
    //POST (/)=> Création (201 CREATED, 404 NOT_FOUND, 409 CONFLICT)
    //PUT (/{id})=> Remplacer (200 OK, 204 NO_CONTENT, 404 NOT_FOUND)
    //PATCH (/{id}/ressource) => Modifier (200, 204, 404)
    //DELETE (/{id}) => Suppression (200, 404)

    private final PlaneService planeService;
    private final PersonService personService;
    private final PlaneTypeRepository planeTypeRepository;

    public PlaneController(PlaneService planeService, PersonService personService, PlaneTypeRepository planeTypeRepository) {
        this.planeService = planeService;
        this.personService = personService;
        this.planeTypeRepository = planeTypeRepository;
    }

    @GetMapping(path = {""}, produces = {"application/xml", "application/json"})
    public ResponseEntity<Page<Plane>> readAllAction(
            @ParameterObject Pageable pageable
//            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
//            @RequestParam(name = "size", defaultValue = "5", required = false) int size
    ) {
        return ResponseEntity.ok(planeService.findAll(pageable));
    }

    @GetMapping(path = {"/{immatriculation}"}, produces = {"application/xml", "application/json"})
    public ResponseEntity<Plane> readOneAction(
            @PathVariable("immatriculation") String immatriculation
    ) {
        Optional<Plane> opt = planeService.findByImmatriculation(immatriculation);

        return opt.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @PostMapping(path = {""})
    @Operation(summary = "Méthode permettant la création d'un avion")
    @SecurityRequirement(name = "jwt")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    content = {
                            @Content(schema = @Schema(implementation = Plane.class), mediaType = "application/json"),
                            @Content(schema = @Schema(implementation = Plane.class), mediaType = "application/xml")
                    }
            ),
            @ApiResponse(
                    responseCode = "404",
                    content = @Content(schema = @Schema(implementation = ExceptionDTO.class))
            ),
            @ApiResponse(
                    responseCode = "409",
                    content = @Content(schema = @Schema(implementation = ExceptionDTO.class))
            ),
            @ApiResponse(
                    responseCode = "412",
                    content = @Content(schema = @Schema(implementation = ExceptionDTO.class))
            )
    })
    @PreAuthorize("hasRole('PLANE_CREATE') || hasRole('AIRPORT_DIRECTOR')")
    public ResponseEntity<Plane> createAction(
            @Valid @RequestBody FPlaneCreate form,
            BindingResult bindingResult
    ) {

        if (bindingResult.hasErrors()) {
            throw new PreConditionException(bindingResult, "Cannot create plane");
        }
        Optional<Plane> opt = planeService.findByImmatriculation(form.imma());
        if (opt.isPresent()) {
            throw new AlreadyExistException("This plane already exists");
        }

        Plane plane = form.toBll();

        Person owner = personService.findOneById(form.ownerId()).orElseThrow(() -> new NotFoundException("Owner Id doesn't exists"));

        PlaneType planeType = planeTypeRepository.findById(form.typeId()).orElseThrow(() -> new NotFoundException("Type id doesn't exists"));

        plane.setOwner(owner);
        plane.setType(planeType);


        Plane created = this.planeService.create(plane);

        return ResponseEntity.status(HttpStatus.CREATED).body(created);
    }

    @PatchMapping(path = {"/{id:[0-9]+}/owner"})
    public ResponseEntity<Plane> changeOwnerAction(
            @PathVariable(name = "id") Plane plane,
            @RequestBody DemoDTO dto
    ) {
        Person opt = personService
                .findOneById(dto.getOwnerId())
                .orElseThrow(() -> new NotFoundException("Owner id doesn't exists"));

        planeService.sellPlane(plane, plane.getOwner(), opt);

        return ResponseEntity.ok(plane);
    }
}
