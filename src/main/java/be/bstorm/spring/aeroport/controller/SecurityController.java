package be.bstorm.spring.aeroport.controller;

import be.bstorm.spring.aeroport.exception.PreConditionException;
import be.bstorm.spring.aeroport.model.dto.security.UserRegisterDTO;
import be.bstorm.spring.aeroport.model.entity.security.SecurityUser;
import be.bstorm.spring.aeroport.model.form.security.FUserRegister;
import be.bstorm.spring.aeroport.services.security.UserService;
import be.bstorm.spring.aeroport.utils.JwtHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class SecurityController {

    private final UserService userService;
    private final JwtHelper jwtHelper;
    private final PasswordEncoder passwordEncoder;

    public SecurityController(UserService userService, JwtHelper jwtHelper, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.jwtHelper = jwtHelper;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(path = {"/register"})
    public ResponseEntity<UserRegisterDTO> registerAction(
            @Valid @RequestBody FUserRegister form,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            throw new PreConditionException(bindingResult, "");
        }

        SecurityUser user = form.toBll();
        user.setPassword(passwordEncoder.encode(form.password()));

        user = this.userService.create(user);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(new UserRegisterDTO(jwtHelper.generateToken(user), user));
    }

    @PostMapping(path = {"/sign-in"})
    public ResponseEntity<UserRegisterDTO> signInAction(
            @Valid @RequestBody FUserRegister form,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            throw new PreConditionException(bindingResult, "");
        }

        SecurityUser user = (SecurityUser) this.userService.loadUserByUsername(form.username());

        if (!passwordEncoder.matches(form.password(), user.getPassword())) {
            throw new PreConditionException(bindingResult, "");
        }
        return ResponseEntity.ok(new UserRegisterDTO(jwtHelper.generateToken(user), user));
    }
}
