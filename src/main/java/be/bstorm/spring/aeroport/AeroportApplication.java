package be.bstorm.spring.aeroport;

import io.swagger.v3.oas.models.annotations.OpenAPI31;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@OpenAPI31
@ConfigurationPropertiesScan
public class AeroportApplication {

    public static void main(String[] args) {
        SpringApplication.run(AeroportApplication.class, args);
    }

}
