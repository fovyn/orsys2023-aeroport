package be.bstorm.spring.aeroport.exception;

import lombok.Data;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

@Data
public class PreConditionException extends RuntimeException {
    private List<FieldError> errors = new ArrayList<>();

    public PreConditionException(BindingResult result, String message) {
        super(message);
        this.errors = result.getFieldErrors();
    }
}
